﻿using Leap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeapMotionFrameworkAdapter
{
    internal interface ILeapEventDelegate
    {
        void LeapEventNotification(string EventName);
    }

    ///// <summary>
    ///// LeapMotion SDK Listener used to trigger device notifications
    ///// </summary>
    //internal class LeapEventListener : Listener
    //{
    //    private ILeapEventDelegate _eventDelegate;

    //    public LeapEventListener(ILeapEventDelegate delegateObject)
    //    {
    //        this._eventDelegate = delegateObject;
    //    }
    //    public override void OnInit(Controller controller)
    //    {
    //        this._eventDelegate.LeapEventNotification("onInit");
    //    }
    //    public override void OnConnect(Controller controller)
    //    {
    //        controller.SetPolicy(Controller.PolicyFlag.POLICY_IMAGES);
    //        controller.EnableGesture(Gesture.GestureType.TYPE_SWIPE);
    //        this._eventDelegate.LeapEventNotification("onConnect");
    //    }
    //    public override void OnFrame(Controller controller)
    //    {
    //        this._eventDelegate.LeapEventNotification("onFrame");
    //    }
    //    public override void OnExit(Controller controller)
    //    {
    //        this._eventDelegate.LeapEventNotification("onExit");
    //    }
    //    public override void OnDisconnect(Controller controller)
    //    {
    //        this._eventDelegate.LeapEventNotification("onDisconnect");
    //    }

    //}
}
