﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MultimodalFramework;

namespace LeapMotionFrameworkAdapter
{
    /// <summary>
    /// This class is a middleware between MultimodalFramework and the client application, providing access to detected Frames, Gestures and Commands
    /// </summary>
    public class LeapMotionManager : Controller, ILeapEventDelegate
    {
        #region Leap Motion SDK Fields

        private Leap.Controller _controller;
        //private LeapEventListener _listener;
        private bool _isClosing = false;

        #endregion

        #region Adapters

        private FrameBuffer _buffer;
        private LeapGestureAdapter _gestureListener;
        private CommandDetector _commandDetector;

        #endregion

        #region Constructor

        public LeapMotionManager(string configPath, string supportedGesturesConfigPath) : base(configPath, supportedGesturesConfigPath)
        {
            // Initializes the buffer
            _buffer = new FrameBuffer(this, 60);

            // Initializes the gesture listener using the buffer and this controller instance (needed for Decoders--and only accessible from the framework, anyway)
            _gestureListener = new LeapGestureAdapter(_buffer, this);

            // Initializes the Command Detector using this controller, its gesture listener, and configuration filepaths
            // can add here the filepath for specific device gestures codenames
            _commandDetector = new CommandDetector(this, _gestureListener, this.ConfigPath, this.SupportedGesturesConfigPath); // use a callback instead of sending Controller as parameter?

            // Initializes Leap Motion SDK Controller
            _controller = new Leap.Controller();

            _controller.Connect += _controller_Connect;
            _controller.Disconnect += _controller_Disconnect;
            _controller.FrameReady += _controller_FrameReady;

            // Initializes Leap Motion SDK Listener creating using this class as the callback for new notifications
            //_listener = new LeapEventListener(this);
            // Adds the listener to the controller
            //_controller.AddListener(_listener);
        }

        private void _controller_FrameReady(object sender, Leap.FrameEventArgs e)
        {
            if (!_isClosing)
                FrameHandler(e.frame);
        }

        private void _controller_Disconnect(object sender, Leap.ConnectionLostEventArgs e)
        {
            Console.WriteLine("Device disconnected.");
            this._isClosing = true;
        }

        private void _controller_Connect(object sender, Leap.ConnectionEventArgs e)
        {
            Console.WriteLine("Device connected.");
            this.connectHandler();
            this._isClosing = false;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Sets policies and general configurations
        /// </summary>
        void connectHandler()
        {
            _controller.SetPolicy(Leap.Controller.PolicyFlag.POLICY_DEFAULT);
            _controller.SetPolicy(Leap.Controller.PolicyFlag.POLICY_BACKGROUND_FRAMES);
            //_controller.EnableGesture(Leap.Gesture.GestureType.TYPE_SWIPE);
            //_controller.Config.SetFloat("Gesture.Swipe.MinLength", 100.0f);
        }

        long _lastFrameID = 0;

        /// <summary>
        /// Handles each frame listened
        /// </summary>
        /// <param name="frame">Leap SDK frame</param>
        void FrameHandler(Leap.Frame frame)
        {
            // Event so client can also receive every frame; probably return both Leap.Frame and LeapFrameAdapter

            if (!this.isConnected() || _lastFrameID == frame.Id)//   TODO add is Valid to Frame? and if (DateTime.Now - LastGesture > new TimeSpan(0, 0, 1))
                return;

            _lastFrameID = frame.Id;

            LeapFrameAdapter adaptee = new LeapFrameAdapter(frame);

            _buffer.Push(adaptee); // (new LeapFrameAdapter(frame))

            if (_buffer.Buffer.Count >= _buffer.BUFFER_SIZE) // TODO ?
            {
                _gestureListener.RunGeneral();
                _gestureListener.RunSwipeCheck();
                _gestureListener.RunHandCheck();
            }
        }

        #endregion

        #region ILeapEventDelegate Implementation

        /// <summary>
        /// LeapEventListener Callback Method
        /// </summary>
        /// <param name="EventName">The name of the triggered event</param>
        public void LeapEventNotification(string EventName)
        {
            switch (EventName)
            {
                case "onInit":
                    Console.WriteLine("Init");
                    break;
                case "onConnect":
                    this.connectHandler();
                    break;
                case "onFrame":
                    if (!_isClosing)
                        this.FrameHandler(_controller.Frame());
                    break;
            }
        }

        #endregion

        #region MultimodalFramework.Controller Implementation

        /// <summary>
        /// Checks whether the controller is connected or not
        /// </summary>
        /// <returns>bool</returns>
        public override bool isConnected()
        {
            return _controller.IsConnected;
        }

        /// <summary>
        /// Retrieve Frame Buffer used in Leap Manager
        /// </summary>
        /// <returns>IFrameBuffer</returns>
        public override FrameBuffer getFrameBuffer()
        {
            return _buffer;
        }

        /// <summary>
        /// Retrieves Gesture Listener used in Leap Manager
        /// </summary>
        /// <returns>GestureDetector.IGestureListener</returns>
        public override GestureListener getGestureListener()
        {
            return _gestureListener;
        }

        public override CommandDetector getCommandListener()
        {
            return _commandDetector;
        }

        // We can override explicit listeners
        //public override GestureDetector.ISwipeListener getSwipeListener()
        //{
        //    return _gestureListener;
        //}

        /// <summary>
        /// Removes listener from leap motion controller and disposes it afterwards
        /// </summary>
        public override void Close()
        {
            _isClosing = true;
            //_controller.RemoveListener(_listener);
            _controller.Dispose();
        }

        #endregion
    }
}
