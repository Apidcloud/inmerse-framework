﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MultimodalFramework;
using MultimodalFramework.Ontologies;

namespace LeapMotionFrameworkAdapter
{
    /// <summary>
    /// This adapter inherits from Frame in order to fill the buffer with frames while still being able to access the data structure from the device SDK
    /// </summary>
    internal class LeapFrameAdapter : OntologicalHandsFrame
    {
        #region Fields

        private Leap.Frame _sdkFrame;

        #endregion

        #region Properties

        public Leap.Frame SDKFrame { get { return _sdkFrame; } private set { _sdkFrame = value; } }

        #endregion

        #region Constructors

        public LeapFrameAdapter(Leap.Frame frame) : base(frame.Id, frame)
        {
            SDKFrame = frame;
        }

        #endregion

        #region OntologicalHandFrame Implementation

        public override OntologicalHand[] GetOntologicalHands(object frame)
        {
            Leap.Frame leapFrame = frame as Leap.Frame;

            // construct ontological hands here
            OntologicalHand[] hands = new OntologicalHand[leapFrame.Hands.Count];

            for (int i = 0; i < leapFrame.Hands.Count; i++)
            {
                Leap.Hand originalHand = leapFrame.Hands[i];
                // only calculate/get normalized palm velocity once
                Leap.Vector palmVelocityNormalized = originalHand.PalmVelocity.Normalized;

                OntologicalPalm palm = new OntologicalPalm(
                    this.GetSwapYZ(originalHand.PalmPosition.Normalized), 
                    this.GetSwapYZ(originalHand.Direction.Normalized), 
                    this.GetSwapYZ(originalHand.PalmNormal.Normalized), 
                    new float[] { palmVelocityNormalized.x, palmVelocityNormalized.y, palmVelocityNormalized.z}
                );

                OntologicalBasicFinger[] fingers = new OntologicalBasicFinger[5];

                for (int j = 0; j < 5; j++)
                {
                    /* 0 = THUMB
				       1 = INDEX
				       2 = MIDDLE
				       3 = RING
				       4 = PINKY
				    */
                    Leap.Finger originalFinger = originalHand.Fingers[j];

                    float[][] bonesPositions = new float[3][];
                    float[][] bonesDirections = new float[3][];

                    // we are not storing metacarpal
                    for (int p = 1; p < originalFinger.bones.Length; p++)
                    {
                        Leap.Bone originalBone = originalFinger.bones[p];

                        // only calculate/get the normalized joints once
                        Leap.Vector nextNormalized = originalBone.NextJoint.Normalized;
                        Leap.Vector prevNormalized = originalBone.PrevJoint.Normalized;

                        float posX = (nextNormalized.x + prevNormalized.x) / 2f;
                        float posY = (nextNormalized.y + prevNormalized.y) / 2f;
                        float posZ = (nextNormalized.z + prevNormalized.z) / 2f;

                        float dirX = nextNormalized.x - prevNormalized.x;
                        float dirY = nextNormalized.y - prevNormalized.y;
                        float dirZ = nextNormalized.z - prevNormalized.z;

                        // not really needed for now
                        //float[] orientation = new float[4] { originalBone.Rotation.x,
                        //                                     originalBone.Rotation.y,
                        //                                     originalBone.Rotation.z,
                        //                                     originalBone.Rotation.z
                        //};

                        // YZ swapped 
                        bonesPositions[p - 1] = new float[3] { -posX, -posZ, -posY };
                        // XY swapped
                        bonesDirections[p - 1] = new float[3] { dirX, dirZ, -dirY };
                    }

                    fingers[j] = new OntologicalBasicFinger(bonesPositions, bonesDirections, originalFinger.Id);
                }

                hands[i] = new OntologicalHand(fingers, palm, originalHand.IsLeft);
            }

            return hands;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Swap axis considering that leap motion is used in HMD mode
        /// </summary>
        /// <param name="vec"></param>
        /// <returns></returns>
        private float[] GetSwapYZ(Leap.Vector vec)
        {
            vec.x = -vec.x;
            float tmp = vec.y;
            vec.y = -vec.z;
            vec.z = -tmp;

            return new float[3] { -vec.x, -vec.z, -vec.y };
        }

        #endregion
    }
}
