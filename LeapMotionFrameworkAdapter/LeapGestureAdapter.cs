﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MultimodalFramework;

namespace LeapMotionFrameworkAdapter
{
    /// <summary>
    /// This Adapter implements the most device suited Gestures interfaces, providing access to gesture detection events
    /// </summary>
    internal class LeapGestureAdapter : GestureListener
    {
        #region Interfaces Events

        //#region General

        //public event GestureDetection.StringEventHandler OnGestureDetected;

        //#endregion

        //#region Swipes

        //public event GestureDetection.EmptyEventHandler OnSwipeRight;
        //public event GestureDetection.EmptyEventHandler OnSwipeLeft;
        //public event GestureDetection.EmptyEventHandler OnSwipeUp;
        //public event GestureDetection.EmptyEventHandler OnSwipeDown;

        //#endregion

        //#region Hand

        //public event GestureDetection.EmptyEventHandler OnGrab;
        //public event GestureDetection.EmptyEventHandler OnOpen;
        //public event GestureDetection.EmptyEventHandler OnPinch;

        //#endregion

        #endregion

        #region Constructor

        public LeapGestureAdapter(FrameBuffer buffer, Controller controller): base(buffer, controller)
        {
        }

        #endregion

        #region GestureDetector.ISwipeListener Implementation

        /// <summary>
        /// Implementation of Activate Detection
        /// </summary>
        public override bool Activate()
        {
            MultimodalFramework.Frame recentFrame = this.FrameBuffer.GetFrame();
            MultimodalFramework.Frame oldFrame = this.FrameBuffer.GetFrame(59);

            if (recentFrame == null || oldFrame == null)
                return false; // invalid frames

            Leap.Frame frame = (recentFrame as LeapFrameAdapter).SDKFrame;
            Leap.Frame oldestFrame = (oldFrame as LeapFrameAdapter).SDKFrame;

            return frame.Hands.Count >= 2 && oldestFrame.Hands.Count == 0;
        }

        /// <summary>
        /// Implementation of Swipe Right Detection
        /// </summary>
        public override bool SwipeRight()
        {
            return IsSwiping(false);
        }

        /// <summary>
        /// Implementation of Swipe Left Detection
        /// </summary>
        public override bool SwipeLeft()
        {
            return IsSwiping(true);
        }

        /// <summary>
        /// Implementation of Swipe Up Detection
        /// </summary>
        //public void SwipeUp()
        //{
        //    // Detect Swipe Up
        //    if (OnSwipeUp != null)
        //    {
        //        // Trigger OnSwipeUp
        //        OnSwipeUp();
        //    }
        //    // Trigger OnGestureDetected sending swipe up info
        //    GestureDetected("swipe_up");
        //}

        /// <summary>
        /// Implementation of Swipe Down Detection
        /// </summary>
        public void SwipeDown()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region GestureDetector.IGrabListener Implementation

        /// <summary>
        /// Implementation of Grab Detection
        /// </summary>
        public override bool Grab()
        {
            MultimodalFramework.Frame recentFrame = this.FrameBuffer.GetFrame();
            MultimodalFramework.Frame oldFrame = this.FrameBuffer.GetFrame(59);

            if (recentFrame == null || oldFrame == null)
                return false; // invalid frames


            Leap.Frame frame = (recentFrame as LeapFrameAdapter).SDKFrame;
            Leap.Frame oldestFrame = (oldFrame as LeapFrameAdapter).SDKFrame;


            if (frame.Hands.Count < 1 || oldestFrame.Hands.Count < 1)
                return false;

            // retrieve first hand (right or left, doesn't really matter)
            Leap.Hand oldestHand = oldestFrame.Hands[0];
            // and same hand
            Leap.Hand hand = frame.Hand(oldestHand.Id);

            if (hand == null)
                return false;

            if (oldestHand.Fingers.FindAll(finger => finger.IsExtended).Count > 4 && oldestHand.GrabStrength < 0.2 && hand.GrabStrength > 0.8)
                return true;

            return false;
        }


        /// <summary>
        /// Implementation of Open Hand Detection
        /// </summary>
        public override bool Open()
        {
            MultimodalFramework.Frame recentFrame = this.FrameBuffer.GetFrame();
            MultimodalFramework.Frame oldFrame = this.FrameBuffer.GetFrame(59);

            if (recentFrame == null || oldFrame == null)
                return false; // invalid frames

            Leap.Frame frame = (recentFrame as LeapFrameAdapter).SDKFrame;
            Leap.Frame oldestFrame = (oldFrame as LeapFrameAdapter).SDKFrame;


            if (frame.Hands.Count < 1 || oldestFrame.Hands.Count < 1)
                return false;

            // retrieve first hand (right or left, doesn't really matter)
            Leap.Hand oldestHand = oldestFrame.Hands[0];
            // and same hand
            Leap.Hand hand = frame.Hand(oldestHand.Id);

            if (hand == null)
                return false;

            if (hand.Fingers.FindAll(finger => finger.IsExtended).Count > 4 && oldestHand.GrabStrength > 0.8 && hand.GrabStrength <= 0.2)
                return true;

            return false;
        }

        /// <summary>
        /// Implementation of Pinch Detection
        /// </summary>
        public void Pinch()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Checks if is swiping according to the input
        /// </summary>
        /// <param name="left">Should search for a swipe left or right</param>
        /// <returns></returns>
        private bool IsSwiping(bool left) // probably return a swipe enum? with every swipe
        {
            // Retrieve the most recent frame
            Leap.Frame frame = (this.FrameBuffer.GetFrame() as LeapFrameAdapter).SDKFrame;

            if (frame == null || frame.Hands.Count < 1)
                return false;

            // retrieve first hand
            Leap.Hand hand = frame.Hands[0];

            if (hand.Fingers.FindAll(finger => finger.IsExtended).Count < 4)
                return false;

            // going right or left?
            bool steppingRight = hand.PalmVelocity[0] >= 0;

            float fAbsX = Math.Abs(hand.Direction.x);
            float fAbsY = Math.Abs(hand.Direction.y);
            float fAbsZ = Math.Abs(hand.Direction.z);

            float handRoll = hand.PalmNormal.Roll;

            //if (fAbsX <= fAbsY || fAbsX <= fAbsZ)
            //    return false;

            if (steppingRight && hand.Direction.x > 0 && handRoll > 0.5)
            {
                return !left;
            } else if (!steppingRight && hand.Direction.x < 0 && handRoll < -0.5)
            {
                return left;
            }

            return false;
        }

        /// <summary>
        /// Used to callback every gesture detected
        /// </summary>
        /// <param name="gesture">The gesture identifier</param>
        private void GestureDetected(string gesture)
        {
        //    if (OnGestureDetected != null)
        //    {
        //        OnGestureDetected(gesture);


        //    }
        }

        #endregion
    }
}
