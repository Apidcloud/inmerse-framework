﻿using System;
using System.Windows;
using System.Reflection;
using System.IO;
// hardcoded way
// using LeapMotionFrameworkAdapter
using MultimodalFramework;
using MultimodalFramework.Utils;
using System.Collections.Generic;
using System.Diagnostics;

namespace LeapMotionWPFTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // hardcoded way
        //private LeapMotionManager _manager;
        
        // dynamic way
        private List<dynamic> _adaptersInstances = null;

        public MainWindow()
        {
            InitializeComponent();

            // Initialize middleware dynamically, so that it can be changed externally, without rebuilding or changing the source code.
            string filepath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            _adaptersInstances = AdapterHelper.LoadAdapters(@".*Adapter\.dll", filepath, @"Resources\Conf.xml", ".\\Resources\\SupportedGesturesCodenames.xml");

            foreach (dynamic adapter in _adaptersInstances)
            {
                // all adapters will have the events redirected to the same functions
                adapter.getGestureListener().OnGestureDetected += new GestureDetection.StringEventHandler(MainWindow_OnGestureDetected);
                adapter.getGestureListener().OnSwipeLeft += new GestureDetection.EmptyEventHandler(MainWindow_OnSwipeLeft);
                adapter.getFrameBuffer().OnFrame += new FrameBuffer.FrameEventHandler(MainWindow_OnFrame);
                adapter.getCommandListener().OnCommandDetected += new GestureDetection.StringEventHandler(MainWindow_OnCommandDetected);
            }

            // Initialize middleware (hardcoded way)
            //_manager = new LeapMotionManager(@"Resources\Conf.xml", ".\\Resources\\SupportedGesturesCodenames.xml");
            //(_manager.getGestureListener() as MultimodalFramework.GestureDetector.ISwipeListener).OnSwipeRight += MainWindow_OnSwipeRight;

            // Register to gesture events
            //_manager.getSwipeListener().OnSwipeRight += MainWindow_OnSwipeRight;
            //_manager.getGestureListener().OnGestureDetected += MainWindow_OnGestureDetected;

            //_manager.getGestureListener().OnSwipeLeft += MainWindow_OnSwipeLeft;

            //_manager.getFrameBuffer().OnFrame += MainWindow_OnFrame;

            //_manager.getCommandListener().OnCommandDetected += MainWindow_OnCommandDetected;
        }

        void MainWindow_OnSwipeLeft()
        {
            //throw new NotImplementedException();
        }

        void MainWindow_OnCommandDetected(string str)
        {
            //throw new NotImplementedException();
            Console.WriteLine("Command Detected: " + str);
        }

        void MainWindow_OnFrame(MultimodalFramework.Frame frame)
        {
            //throw new NotImplementedException();
        }

        void MainWindow_OnGestureDetected(string str)
        {
            if (str.ToLower() == "recordingended")
            {
                Console.WriteLine("Closing application...");
                Application.Current.Shutdown();
            }
        }

        void _manager_OnOpen()
        {
            //Console.WriteLine("open gesture detected");
            //throw new NotImplementedException();
        }

        void _manager_OnGrab()
        {
            //Console.WriteLine("grab gesture detected");
            //throw new NotImplementedException();
        }

        void MainWindow_OnSwipeRight()
        {
            //throw new NotImplementedException();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            foreach (dynamic adapter in _adaptersInstances)
            {
                // also guaranteed to exist given the abstract controller class
                adapter.Close();
            }
        }

        
    }
}
