# README #

### InMERSE Framework ###

Framework for multimodal gestural input. You can use it to develop applications that are independent from gesture-recognition devices and gesture-recognition methods.

This is an ongoing project, and help to improve it and complete is welcomed.

Code under the LGPL license:
[https://en.wikipedia.org/wiki/GNU_Lesser_General_Public_License](Link URL)

The concepts and description are found in paper:
"Separating Gesture Detection and Application Control Concerns with a Multimodal Architecture", by Morgado et al. (2015), presented at the VEAI workshop of the 2015 IEEE IUCC conference.
[https://www.researchgate.net/publication/283343525_Separating_Gesture_Detection_and_Application_Control_Concerns_with_a_Multimodal_Architecture](Link URL)

This framework was originally created as part of project InMERSE, developed in Portugal, at the INESC TEC research lab, with the cooperation and funding of PT Inovação (now Altice Labs).

Background on what can be achieved is provided in early (no software) papers:

* Morgado, Leonel (2014). Cultural Awareness and Personal Customization of Gestural Commands Using a Shamanic Interface, *Procedia Computer Science*, 27, 449 - 459.
  [http://www.sciencedirect.com/science/article/pii/S1877050914000519](Link URL)

* Morgado, Leonel et al. (2015). Cities in Citizens’ Hands, *Procedia Computer Science*, 67, 430 - 438.
  [https://www.researchgate.net/publication/278023915_Cities_in_citizens_hands](Link URL)

## Setup
The adapter example (LeapMotionFrameworkAdapter) is dependant on .NET 4.5, and  Leap Motion Orion SDK 3.2.1+45911 (unzip and install from LeapMotionFrameworkAdapter/Dependencies/). You can alternatively download it from the [archive](https://developer-archive.leapmotion.com/get-started?id=v3-developer-beta&platform=windows&version=3.2.1.45911) of leap motion. This was the last version leap motion has released supporting 'Desktop' C#. Versions 4.x onwards only support C and Unity (through the former).

Other adapters (e.g., for Microsoft Kinect, or Myo Gesture) can be used in a plug-n-play fashion by making the main consumer application (e.g., LeapMotionWPFTest) dependant on Commands rather than on specific actions such as Gestures. Doing so allows it to work without any changes to its source code or the need of rebuilding it. Simply dropping a valid .dll in the executable directory with a name ending in 'Adapter' (e.g., LeapMotionFrameworkAdapter.dll) that derives from MultimodalFramework (i.e., has a subclass implementing Controller such as LeapMotionManager) is enough. 

# Contacts #
### INESC TEC ###
Luís Fernandes - lfernandes@utad.pt
Ricardo Rodrigues Nunes - rrnunes@utad.pt

### Universidade Aberta ###
Leonel Morgado - leonel.morgado@uab.pt

### Universidade de Trás-os-Montes e Alto Douro ###
Hugo Paredes - hparedes@utad.pt
Luís Barbosa - lfb@utad.pt
Paulo Martins - pmartins@utad.pt
Benjamim Fonseca

### AlticeLabs ###
Fausto de Carvalho - cfausto@telecom.pt
Bernardo Cardoso - bernardo@telecom.pt
