﻿using MultimodalFramework.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace MultimodalFramework
{
    public abstract class GestureListener
    {
        #region Consts

        private const string SUFFIX_FOR_DECODER_ASSEMBLY = "Decoder";
        private const string SUFFIX_FOR_DECODER_CONFIG = "Config";
        private const string CONFIG_FOLDER_NAME = "Resources";

        #endregion

        #region Fields

        private readonly Controller _controller;

        // Probably not needed
        DateTime _latest = DateTime.Now.AddSeconds(-1);

        List<dynamic> _decodersInstances = null;

        #endregion

        #region Properties

        public FrameBuffer FrameBuffer { get; protected set; }

        #endregion

        #region Events

        public event GestureDetection.StringEventHandler OnGestureDetected;

        public event GestureDetection.EmptyEventHandler OnSwipeRight;
        public event GestureDetection.EmptyEventHandler OnSwipeLeft;

        public event GestureDetection.EmptyEventHandler OnGrab;
        public event GestureDetection.EmptyEventHandler OnOpen;

        public event GestureDetection.EmptyEventHandler OnActivate;

        #endregion

        #region Abstract Methods

        public abstract bool SwipeRight();
        public abstract bool SwipeLeft();
        public abstract bool Grab();
        public abstract bool Open();
        public abstract bool Activate();

        #endregion

        #region Constructors

        public GestureListener(FrameBuffer buffer, Controller controller, List<dynamic> decoders = null)
        {
            this._controller = controller;

            this.FrameBuffer = buffer;

            this._decodersInstances = decoders == null ? this.LoadDecodersDynamically() : decoders;

            foreach (dynamic decoder in _decodersInstances)
            {
                // all decoders will have the events redirected to the same functions
                decoder.OnDecoderGestureDetected += new GestureDetection.StringEventHandler(OnDecoderGestureDetected);
            }

            this.FrameBuffer.OnFrame += FrameBuffer_OnFrame;
        }

        #endregion

        #region Subscribed Events

        private void FrameBuffer_OnFrame(Frame frame)
        {
            if (_decodersInstances == null)
                return;

            // send incoming frames to all decoders (if any)
            foreach (dynamic decoder in _decodersInstances)
            {
                decoder.OnFrameReady(frame);
            }
        }

        private void OnDecoderGestureDetected(string str)
        {
            // make sure controller is set to detection mode or that it is looking for this particular escape gesture
            if (this._controller.Mode == ControllerModes.Detection || this._controller.Escape.Equals(str))
            {
                if (OnGestureDetected != null)
                {
                    OnGestureDetected(str);
                }
            }
        }

        #endregion

        #region Virtual Methods

        public virtual void RunGeneral()
        {
            // Probably not needed
            if (DateTime.Now - _latest < new TimeSpan(0, 0, 1)) // timeout
            {
                //return;
            }

            if (Activate())
            {
                FrameBuffer.Flush();
                _latest = DateTime.Now;
                if (this._controller.Mode == ControllerModes.Detection || this._controller.Escape.Equals("activate"))
                {
                    if (OnActivate != null)
                    {
                        OnActivate();
                    }
                    if (OnGestureDetected != null)
                    {
                        OnGestureDetected("activate");
                    }
                }
            }
        }

        /// <summary>
        /// This template method must be called by the implementation
        /// </summary>
        public virtual void RunSwipeCheck() // virtual ??
        {
            if (DateTime.Now - _latest < new TimeSpan(0, 0, 1)) // timeout
            {
                return;
            }

            if (SwipeRight())
            {
                _latest = DateTime.Now;
                if (this._controller.Mode == ControllerModes.Detection || this._controller.Escape.Equals("swipe_right"))
                {
                    if (OnSwipeRight != null)
                    {
                        OnSwipeRight();
                    }
                    if (OnGestureDetected != null)
                    {
                        OnGestureDetected("swipe_right");
                    }
                }
            }

            if (SwipeLeft())
            {
                _latest = DateTime.Now;
                if (this._controller.Mode == ControllerModes.Detection || this._controller.Escape.Equals("swipe_left"))
                {
                    if (OnSwipeLeft != null)
                    {
                        OnSwipeLeft();
                    }
                    if (OnGestureDetected != null)
                    {
                        OnGestureDetected("swipe_left");
                    }
                }
            }
        }

        public virtual void RunHandCheck() // virtual ??
        {
            if (DateTime.Now - _latest < new TimeSpan(0, 0, 1))
            {
                //return;
            }

            if (Grab())
            {
                FrameBuffer.Flush();
                _latest = DateTime.Now;
                if (this._controller.Mode == ControllerModes.Detection || this._controller.Escape.Equals("grab"))
                {
                    if (OnGrab != null)
                    {
                        OnGrab();
                    }
                    if (OnGestureDetected != null)
                    {
                        OnGestureDetected("grab");
                    }
                }
            }

            if (Open())
            {
                FrameBuffer.Flush();
                _latest = DateTime.Now;
                if (this._controller.Mode == ControllerModes.Detection || this._controller.Escape.Equals("open"))
                {
                    if (OnOpen != null)
                    {
                        OnOpen();
                    }
                    if (OnGestureDetected != null)
                    {
                        OnGestureDetected("open");
                    }
                }
            }
        }

        #endregion

        #region Private Methods

        private List<dynamic> LoadDecodersDynamically()
        {
            string baseFilepath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string assemblyPattern = ".*" + SUFFIX_FOR_DECODER_ASSEMBLY + @"\.dll";
            string configurationPattern = ".*" + SUFFIX_FOR_DECODER_ASSEMBLY + SUFFIX_FOR_DECODER_CONFIG + @"\.xml";

            Regex pattern = new Regex(configurationPattern);

            string[] configurations = Directory.GetFiles(baseFilepath + Path.DirectorySeparatorChar + CONFIG_FOLDER_NAME).Where(path => pattern.IsMatch(path)).ToArray();

            if (configurations.Length == 0)
                throw new FileNotFoundException("A file with the pattern " + configurationPattern +
                    " has not been found. Be sure to include it in a subfolder named " + CONFIG_FOLDER_NAME + " that is next to the executable.");

            return AdapterHelper.LoadDecoders(assemblyPattern, baseFilepath, configurations[0]);
        }

        #endregion

        #region Internal Methodss

        internal List<string> GetDecodersSupportedGestures()
        {
            if (_decodersInstances == null)
                return null;

            List<string> allDecodersSupportedGesturesMerged = new List<string>();

            foreach (dynamic decoder in _decodersInstances)
            {
                allDecodersSupportedGesturesMerged.AddRange(decoder.GetDecoderSupportedGestures());
            }
            return allDecodersSupportedGesturesMerged;
        }

        #endregion
    }
}
