﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.IO;
using System.Reflection;

namespace MultimodalFramework.Utils
{
    public static class AdapterHelper
    {
        public static List<dynamic> LoadAdapters(string patternString = @".*Adapter\.dll", string searchFilePath = "./", 
            string commandConfig = @"Resources/Conf.xml", string supportedGesturesConfig = @"Resources/SupportedGesturesCodenames.xml")
        {
            return AssemblyHelper.LoadAssemblies(patternString, searchFilePath, typeof(Controller), commandConfig, supportedGesturesConfig);
        }

        internal static List<dynamic> LoadDecoders(string patternString = @".*Decoder\.dll", string searchFilePath = "./", string gestureConfigFilePath = "./")
        {
            return AssemblyHelper.LoadAssemblies(patternString, searchFilePath, typeof(Decoder), gestureConfigFilePath);
        }
    }
}
