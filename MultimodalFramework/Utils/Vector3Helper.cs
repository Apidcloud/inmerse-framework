﻿using System;

namespace MultimodalFramework.Utils
{
    public static class Vector3Helper
    {
        public static float[] Vector3ProjectOnPlane(float[] vec, float[] planeNormal)
        {
            if (vec.Length != 3 || planeNormal.Length != 3)
                return null;

            return Vector3Subtraction(vec, Vector3ProjectOnVector(vec, planeNormal));
        }

        public static float[] Vector3ProjectOnVector(float[] vec1, float[] vec2)
        {
            if (vec1.Length != 3 || vec2.Length != 3)
                return null;

            float denominator = Vector3MagnitudeSqr(vec2);

            if (denominator == 0)
                return null;

            float scalar = Vector3Dot(vec2, vec1) / denominator;

            return Vector3ScalarMultiplication(vec2, scalar);
        }

        public static float[] Vector3Subtraction(float[] vec1, float[] vec2)
        {
            if (vec1.Length != 3 || vec2.Length != 3)
                return null;

            return new float[3] { vec1[0] - vec2[0], vec1[1] - vec2[1], vec1[2] - vec2[2] };
        }

        public static float Vector3Magnitude(float[] vec)
        {
            if (vec.Length != 3)
                return -1f;

            return (float)Math.Sqrt(vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2]);
        }

        public static float Vector3MagnitudeSqr(float[] vec)
        {
            if (vec.Length != 3)
                return -1f;

            return vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2];

        }

        public static float[] Vector3ScalarMultiplication(float[] vec, float scalar)
        {
            if (vec.Length != 3)
                return null;

            return new float[3] { vec[0] * scalar, vec[1] * scalar, vec[2] * scalar };
        }

        public static float Vector3Dot(float[] vec1, float[] vec2)
        {
            return (vec1[0] * vec2[0]) + (vec1[1] * vec2[1]) + (vec1[2] * vec2[2]);
        }

        public static float[] Vector3CrossProduct(float[] vec1, float[] vec2)
        {
            if (vec1.Length != 3 || vec2.Length != 3)
                return null;

            return new float[3]{(vec1[1] * vec2[2]) - (vec1[2] * vec2[1]),
                          (vec1[2] * vec2[0]) - (vec1[0] * vec2[2]),
                          (vec1[0] * vec2[1]) - (vec1[1] * vec2[0]) };
        }
    }
}
