﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace MultimodalFramework.Utils
{
    internal static class AssemblyHelper
    {
        internal static List<dynamic> LoadAssemblies(string patternString, string searchFilePath, Type assemblyType, params object[] paramsList)
        {
            List<dynamic> dynamicAdapters = new List<dynamic>();

            Regex pattern = new Regex(patternString);

            string[] assemblies = Directory.GetFiles(searchFilePath).Where(path => pattern.IsMatch(path)).ToArray();

            if (assemblies.Length == 0)
                throw new DllNotFoundException("No assembly could be found with the given pattern, search file path.");

            // dynamically initializing
            foreach (var adapter in assemblies)
            {
                // try to find controller descendant
                foreach (Type type in Assembly.LoadFile(adapter).GetExportedTypes())
                {
                    if (type.IsSubclassOf(assemblyType))
                    {
                        // all instances are sure to follow this signature (string commandsConfig, string supportedGesturesCodenamesConfig)
                        dynamicAdapters.Add(Activator.CreateInstance(type, paramsList));
                    }
                }
            }

            return dynamicAdapters;
        }
    }
}
