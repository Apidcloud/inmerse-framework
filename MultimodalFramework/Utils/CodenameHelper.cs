﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MultimodalFramework.Utils
{
    public static class CodenameHelper
    {
        public static List<string> GetGesturesCodenames(string configPath)
        {
            if (!System.IO.File.Exists(configPath) || Path.GetExtension(configPath).ToLower() != ".xml")
            {
                throw new ArgumentException("The provided configuration file path isn't valid, or is not a .xml file.");
            }

            XDocument doc = XDocument.Load(configPath);

            var gestures = from c in doc.Element("SupportedGesturesCodenames").Descendants("Gesture") select c;

            List<string> supportedGestures = new List<string>();

            foreach (var member in gestures)
            {
                supportedGestures.Add((member.Attribute("codename").Value.Trim().ToLower()));
            }

            return supportedGestures;
        }

    }
}
