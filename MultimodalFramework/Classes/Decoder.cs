﻿using MultimodalFramework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultimodalFramework
{
    public abstract class Decoder
    {
        #region Fields

        protected List<string> _decoderSupportedGestures = new List<string>();

        private string _decoderSupportedGesturesFilepath;

        #endregion

        #region Events

        public event GestureDetection.StringEventHandler OnDecoderGestureDetected;

        #endregion

        #region Constructors

        public Decoder(string decoderSupportedGesturesFilepath)
        {
            this._decoderSupportedGesturesFilepath = decoderSupportedGesturesFilepath;
        }

        #endregion

        #region Abstract Methods

        // Virtual is ok here because there might be some other file the decoders want to use (other than xml, etc.)
        public virtual List<string> GetDecoderSupportedGestures()
        {
            this._decoderSupportedGestures = CodenameHelper.GetGesturesCodenames(this._decoderSupportedGesturesFilepath);
            return this._decoderSupportedGestures;
        }

        public abstract void OnFrameReady(Frame frame);

        #endregion

        #region Virtual Methods

        protected virtual void InvokeDecoderGestureDetectedEvent(string str)
        {
            OnDecoderGestureDetected?.Invoke(str);
        }

        #endregion
    }
}
