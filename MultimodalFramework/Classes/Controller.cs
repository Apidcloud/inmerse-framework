﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultimodalFramework
{
    public enum ControllerModes { Detection, Acquisition }

    public delegate void ModeEventHandler(ControllerModes changedTo);

    public abstract class Controller
    {
        #region Fields

        private ControllerModes _mode = ControllerModes.Detection;
        private string _escape = string.Empty;

        #endregion

        #region Properties

        protected string ConfigPath { get; set; }
        protected string SupportedGesturesConfigPath { get; set; }

        public ControllerModes Mode
        {
            get { return _mode; }
            protected set { _mode = value; } // TODO: or private?
        }

        public string Escape { get { return _escape; } private set { _escape = value; } }

        #region Events

        public event ModeEventHandler OnModeChanged;

        #endregion

        #endregion

        #region Constructors

        protected Controller(string configPath, string supportedGesturesConfigPath)
        {
            ConfigPath = configPath;
            SupportedGesturesConfigPath = supportedGesturesConfigPath;
        }

        #endregion

        #region Abstract Methods

        public abstract bool isConnected();

        public abstract FrameBuffer getFrameBuffer();

        public abstract GestureListener getGestureListener();

        public abstract CommandDetector getCommandListener();

        public abstract void Close();

        #endregion

        #region Virtual Methods

        public virtual GestureDetection.ISwipeListener getSwipeListener()
        {
            // returns null if cast fails?
            return getGestureListener() as GestureDetection.ISwipeListener;
        }

        public virtual GestureDetection.ITapListener getTapListener()
        {
            // returns null if cast fails?
            return getGestureListener() as GestureDetection.ITapListener;
        }

        public virtual GestureDetection.IGrabListener getGrabListener()
        {
            // returns null if cast fails?
            return getGestureListener() as GestureDetection.IGrabListener;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Attempts to change Controller Mode
        /// </summary>
        /// <param name="changeTo">Requested mode</param>
        /// <returns>Whether changed the mode or not.</returns>
        public bool ChangeMode(ControllerModes changeTo, string escape = "")
        {
            if (Mode != changeTo)
            {
                this.Escape = escape;
                // TODO: If is in the middle of a command should it be able to change...? wouldn't that make hanging the application possible, though?
                // Controller has priority over client ?
                Mode = changeTo;
                Console.WriteLine("Changed to: " + Mode.ToString());
                Console.WriteLine("Waiting for: " + this.Escape);

                // Trigger OnModeChanged
                if (OnModeChanged != null)
                    OnModeChanged(Mode);

                return true;
            }
            return false;
        }

        #endregion

    }
}
