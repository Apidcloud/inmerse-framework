﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultimodalFramework
{
    [Serializable]
    public abstract class OntologicalBodiesFrame : Frame
    {
        #region Properties
        //public OntologicalBody[] Bodies { get; private set; }

        #endregion

        public OntologicalBodiesFrame(long frameId, object frame) : base(frameId)
        {
            // call of implementation to get the device specific transformation to ontological bodies
            // this avoids outside of framework function calls (that are framework's job, anyway)
            // this requires the framework to actually receive the original device SDK frame object,
            // but the alternative would be having the subclass knowing what to call and when, which ultimately we should avoid 
            //this.Bodies = this.GetOntologicalBodies(frame);
            throw new NotImplementedException("This ontology has not been implemented yet.");
        }

        #region Abstract Methods

        //public abstract OntologicalBody[] GetOntologicalBodies(object frame);

        #endregion
    }
}
