﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultimodalFramework
{
    /// <summary>
    /// This class provides a Limited Size Buffer used to store the latest Frames
    /// </summary>
    public class FrameBuffer // implement IEnumerable to iterate? drawbacks: list is constantly removing... exceptions?
    {
        public delegate void FrameEventHandler(Frame frame);

        #region Events

        public event FrameEventHandler OnFrame;

        #endregion

        #region Readonly

        /// <summary>
        /// The buffer size limit
        /// </summary>
        public readonly int BUFFER_SIZE;

        #endregion

        #region Fields

        /// <summary>
        /// The Buffer
        /// </summary>
        private List<Frame> _buffer = new List<Frame>();

        /// <summary>
        /// Controller
        /// </summary>
        private Controller _controller;

        #endregion

        #region Properties

        public List<Frame> Buffer // virtual, too?
        {
            get
            {
                return _buffer;
            }
            private set
            {
                _buffer = value;
            }
        }

        #endregion

        #region Constructor

        public FrameBuffer(Controller controller, int bufferSize = 60)
        {
            BUFFER_SIZE = bufferSize;

            _controller = controller;

            //for (int i = 0; i < BUFFER_SIZE; i++)
            //{
            //    Buffer.Add(new Frame(false)); // add empty and invalid frames to prevent crashes
            //}

        }

        #endregion

        #region Virtual Methods

        /// <summary>
        /// Pushes new Frame to the end of the limited sized List
        /// </summary>
        /// <param name="frame">The frame to Push</param>
        public virtual void Push(Frame frame)
        {
            // process frame first
            if (_buffer.Count >= BUFFER_SIZE)
            {
                _buffer.RemoveAt(0);
            }
            _buffer.Add(frame);

            // then trigger events if applicable
            // before: Only trigger the event if 1) there are listeners and 2) Controller Mode is Acquisition
            // This is not really true, as decoders need permanent incoming raw data to process and detect gestures (detection mode)
            if (OnFrame != null /**&& _controller.Mode == ControllerModes.Acquisition **/)
            {
                OnFrame(frame);
            }
        }

        //public Frame Pop()
        //{
        //    Frame frame = _buffer[_buffer.Count - 1];

        //    return _buffer.Pop();
        //}

        /// <summary>
        /// Retrieves the most recent Frame
        /// </summary>
        public virtual Frame GetFrame()
        {
            return GetFrame(0);
        }

        /// <summary>
        /// Retrieves Frame from history
        /// </summary>
        /// <param name="index"></param>
        /// <returns>Returns the searched Frame if found an invalid Frame otherwise</returns>
        public virtual Frame GetFrame(int index)
        {
            int elementPosition = _buffer.Count - 1 - index;

            if (elementPosition < 0)
            {
                return null;
            }

            return _buffer[elementPosition]; // Return indexth element from newer to older frames
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Flushes the buffer
        /// </summary>
        public void Flush()
        {
            Buffer.Clear();
        }

        #endregion
    }
}
