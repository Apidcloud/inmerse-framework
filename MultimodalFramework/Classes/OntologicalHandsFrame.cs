﻿using MultimodalFramework.Ontologies;
using System;

namespace MultimodalFramework
{
    [Serializable]
    public abstract class OntologicalHandsFrame : Frame
    {
        public OntologicalHand[] Hands { get; private set; }

        #region Constructors

        public OntologicalHandsFrame(long frameId, object frame) : base(frameId)
        {
            // call of implementation to get the device specific transformation to ontological hands
            // this avoids outside of framework function calls (that are framework's job, anyway)
            this.Hands = this.GetOntologicalHands(frame);
        }

        #endregion

        #region Abstract Methods

        public abstract OntologicalHand[] GetOntologicalHands(object frame);

        #endregion
    }
}
