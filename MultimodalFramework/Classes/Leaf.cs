﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultimodalFramework
{
    /// <summary>
    /// The 'Leaf' class which in this case will be a 'Command'
    /// </summary>
    public class Leaf : Component
    {
        #region Fields

        private string _hexaCode = "";
        //private string _escape = ""; // 

        //public string Escape
        //{
        //    get { return _escape; }
        //    set { _escape = value; }
        //}
        

        #endregion

        #region Properties

        public string HexaCode
        {
            get { return _hexaCode; }
            set { _hexaCode = value; }
        }

        #endregion

        #region Constructor

        public Leaf(string name) : base(name)
        {
        }

        #endregion

        #region Abstract Methods Implementation

        public override Component Add(Component c)
        {
            Console.WriteLine("Cannot add to a leaf");
            throw new NotImplementedException();
        }

        public override void Remove(Component c)
        {
            Console.WriteLine("Cannot remove from a leaf");
            throw new NotImplementedException();
        }

        public override void Display(int depth)
        {
            Console.WriteLine(new String('-', depth) + Name + " - Hexacode: " + HexaCode + " - Size in Bytes: " + HexaCode.Length * sizeof(Char));

            //byte[] bytes = CommandDetector.ConvertToByteArray("0x0123456789AbCdEf");
        }

        #endregion
    }
}
