﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using MultimodalFramework.Utils;

namespace MultimodalFramework
{
    public class CommandDetector
    {
        #region Consts

        private const string HEXA_FORMAT = "X2";

        private const string DETECTION_KEY = "detection";//ControllerModes.Detection.ToString().ToLower();
        private const string ACQUISITION_KEY = "acquisition"; //ControllerModes.Aquisition.ToString().ToLower();

        private readonly string DETECTION_VALUE = string.Empty;
        private readonly string ACQUISITION_VALUE = string.Empty;

        #endregion

        #region Fields

        private string _chainCode;

        // The root of the tree
        private Composite _root = new Composite("root"); // only needed if using tree
        
        //public Leaf Translate(Gesture)
        private List<Leaf> _allCommands = new List<Leaf>();

        private readonly Controller _controller;

        #region LookupTable

        List<string> _codenames;

        Dictionary<string, string> _codenamesLookup;

        #endregion

        #endregion

        #region Events

        public event GestureDetection.StringEventHandler OnCommandDetected;

        #endregion

        #region Constructor

        //  Controller controller, GestureListener
        // GestureListener could be obtained from controller (controller.GetGestureListener()),
        // but for that to happen, CommandDetector would have to be created only after the gesture listener.
        // To make sure that order is actually followed, we ask for the gesture listener here specifically.
        public CommandDetector(Controller controller, GestureListener gestureListener, string frameworkConfigPath, string supportedGesturesConfigPath = "@External/SupportedGesturesCodenames.xml")
        {
            // store controller for later
            _controller = controller;

            // codenames can also be added. Each hexacode will be generated automatically
            // these are basically the supported codenames for the configuration file
            _codenames = new List<string>()
            {
                DETECTION_KEY,
                ACQUISITION_KEY,
            };
            // get framework specific gestures codenames
            _codenames.AddRange(CodenameHelper.GetGesturesCodenames(supportedGesturesConfigPath));

            // get decoders specific gestures codenames
            _codenames.AddRange(gestureListener.GetDecodersSupportedGestures());

            // get device specific gestures codenames
            //_codenames.AddRange(CodenameHelper.GetGesturesCodenames(deviceSupportedGesturesConfigPath));

            // prevent duplicates
            _codenames = _codenames.Distinct().ToList();

            _codenamesLookup = CreateLookupTable(_codenames);

            // Calculate detection and acquisition values
            DETECTION_VALUE = _codenamesLookup[DETECTION_KEY];
            ACQUISITION_VALUE = _codenamesLookup[ACQUISITION_KEY];

            _chainCode = string.Empty;
            // Create Tree or Graph using the config file
            //System.Diagnostics.Stopwatch stopwatch = System.Diagnostics.Stopwatch.StartNew();
            //GenerateTree(pathConfig);
            this.GenerateCommandsHexaCodes(frameworkConfigPath);
            //System.Threading.Thread.Sleep(500);
            //stopwatch.Stop();
            //Console.WriteLine(stopwatch.ElapsedMilliseconds);

            // listen to gestures
            gestureListener.OnGestureDetected += CommandDetector_OnGestureDetected;

            //CommandDetector_OnGestureDetected("Grab");
        }

        #endregion

        #region Subscribed Events Methods

        void CommandDetector_OnGestureDetected(string str)
        {
            // change mode back after 'escaping'
            if (_controller.Escape.Equals(str))
            {
                if (_controller.Mode == ControllerModes.Detection)
                {
                    _controller.ChangeMode(ControllerModes.Acquisition);
                }
                else if (_controller.Mode == ControllerModes.Acquisition)
                {
                    _controller.ChangeMode(ControllerModes.Detection);
                }

                return; // prevent from starting a sequence based on 'escape' gesture
            }

            string gestureCodename = str.Trim().ToLower();

            if (!_codenamesLookup.ContainsKey(gestureCodename))
            {
                Console.WriteLine("Detected " + str + " Gesture, but it is not registered as a command in the configuration file.");
                return;
            }

            _chainCode += _codenamesLookup[gestureCodename];

            bool reset = true;

            // try to find sequence
            foreach (var command in _allCommands)
            {
                string substring = string.Empty;
                if (_chainCode.Length <= command.HexaCode.Length)
                {
                    substring = command.HexaCode.Substring(0, _chainCode.Length);
                }
                else
                {
                    continue;
                }

                if (substring.Equals(_chainCode) && _chainCode.Length == command.HexaCode.Length) // command detected
                {
                    Console.WriteLine("Command '" + command.Name + "' detected.");

                    // TODO create method
                    // Trigger OnCommandDetected
                    if (OnCommandDetected != null)
                    {
                        OnCommandDetected(command.Name);
                    }
                    // reset chain?
                    reset = true;
                    break; // ??

                }
                else if (substring.Equals(_chainCode)) // sub-match found
                {
                    // check if the next codenames correspond to a mode change and consequent escape
                    if (_chainCode.Length + 4 <= command.HexaCode.Length) // +4 since each code has 2 digits
                    {
                        string possibleMode = command.HexaCode.Substring(_chainCode.Length, 2);
                        string possibleEscape = command.HexaCode.Substring(_chainCode.Length + 2, 2);

                        ControllerModes? mode = null;

                        if (possibleMode == DETECTION_VALUE)
                        {
                            mode = ControllerModes.Detection;
                        }
                        else if (possibleMode == ACQUISITION_VALUE)
                        {
                            mode = ControllerModes.Acquisition;
                        }

                        if (mode != null)
                        {
                            // listen to only this gesture
                            string escapeCodename = _codenamesLookup.FirstOrDefault(x => x.Value == possibleEscape).Key;

                            // ...

                            Console.WriteLine("Command '" + command.Name + "' detected");
                            // Trigger OnCommandDetected ?
                            if (OnCommandDetected != null)
                            {
                                OnCommandDetected(command.Name);
                            }

                            _controller.ChangeMode((ControllerModes)mode, escapeCodename);

                            // reset chain?
                            reset = true;
                            break; // ??

                        }

                    }

                    // keep searching
                    reset = false;
                    break;
                }
            }

            if (reset) // if no matches were found
            {
                // reset
                _chainCode = string.Empty; 
            }

        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Creates a dictionary based on every codename
        /// </summary>
        private Dictionary<string, string> CreateLookupTable(List<string> codenames)
        {
            Dictionary<string, string> lookup = new Dictionary<string, string>();
            foreach (var codename in codenames)
            {
                // codename => hexa_value
                lookup.Add(codename.Trim().ToLower(), codenames.IndexOf(codename).ToString(HEXA_FORMAT));
            }
            return lookup;
        }

        /// <summary>
        /// Generate a Tree based on the xml file
        /// </summary>
        /// <param name="path"></param>
        void GenerateTree(string path)
        {
            if (!System.IO.File.Exists(path))
            {
                throw new ArgumentException("The provided configuration file path isn't valid.");
            }

            XDocument doc = XDocument.Load(path);

            var commands = from c in doc.Element("Commands").Descendants("Command") select c;

            Component parent = _root;

            foreach (var member in commands)
            {
                Leaf command = new Leaf(member.Attribute("name").Value);

                var sequence = from g in member.Descendants("Gesture") select g;

                foreach (var part in sequence)
                {
                    string gestureCodeName = part.Attribute("codename").Value.Trim().ToLower();
                    // Create Composite and add to parent node
                    // change parent node to the one added or to the one with the same codename (returned by Add)
                    Composite gesture = new Composite(gestureCodeName);
                    parent = parent.Add(gesture);
                }

                // Add Leaf to latest parent
                parent.Add(command);

                // then reset parent to the tree root
                parent = _root;
            }

            // Print Tree in console
            _root.Display(1);
        }

        /// <summary>
        /// Generate HexaCodes based on xml file
        /// </summary>
        /// <param name="path"></param>
        void GenerateCommandsHexaCodes(string path)
        {
            if (!File.Exists(path) || Path.GetExtension(path).ToLower() != ".xml")
            {
                throw new ArgumentException("The provided configuration file path isn't valid, or is not a .xml file.");
            }

            XDocument doc = XDocument.Load(path);

            var commands = from c in doc.Element("Commands").Descendants("Command") select c;

            foreach (var member in commands)
            {
                Leaf command = new Leaf(member.Attribute("name").Value);

                var sequence = from g in member.DescendantNodes() select g; //Descendants("Gesture") select g;

                bool abort = false;

                foreach (var part in sequence)
                {
                    // DescendantNodes returns comments, elements, etc.
                    // In this case, we only need two elements: Gesture and Mode
                    if (part.NodeType != System.Xml.XmlNodeType.Element)
                        continue;

                    string partCodename = String.Empty;
                    string escapeCodename = String.Empty;

                    XElement element = (part as XElement);

                    switch (element.Name.ToString().Trim().ToLower()) // TODO no need for a switch?
                    {
                        case "gesture":

                            // Try to Concatenate Gesture Codename
                            if (!this.SumHexa(command, element.Attribute("codename").Value))
                            {
                                abort = true;
                                break;
                            }

                            XElement mode = element.Element("Mode");

                            if (mode != null)
                            {

                                if (!this.SumHexa(command, mode.Attribute("enter").Value))
                                {
                                    continue; // continue or abort?
                                }

                                if (!this.SumHexa(command, mode.Attribute("escape").Value))
                                {
                                    this.Rollback(command); // rollback enter codename
                                    continue; // continue or abort?
                                }
                            }
                            break;
                        default:
                            // TODO: skip this part or abort?
                            continue;
                    }
                }

                // if everything was considered valid, try to add command
                if (!abort)
                {
                    // Sequence Codes must be unique. Names might be repeated, though. ??
                    if (_allCommands.Find(x => x.HexaCode == command.HexaCode) == null)
                    {
                        // Add since this sequence doesn't exist
                        _allCommands.Add(command);
                    }
                }

            }

            // Print Codes in console
            _allCommands.ForEach(x => x.Display(1));
        }


        /// <summary>
        /// Attempts to concatenate the codename hexadecimal value to the specified command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="codename"></param>
        /// <returns>False when the codename couldn't be found and true when concatenated successfully</returns>
        bool SumHexa(Leaf command, string codename)
        {
            string code = codename.Trim().ToLower();

            if (_codenamesLookup.ContainsKey(code)) // found a match
            {
                string hexaCode = _codenamesLookup[code];
                // Convert to hexadecimal with 2 digits » 256 different codenames...
                command.HexaCode += hexaCode;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Attemps to rollback command hexacode
        /// </summary>
        /// <param name="command">The command to rollback</param>
        /// <param name="times">The number of times to rollback</param>
        /// <returns>False when couldn't rollback the number of times specified and true otherwise</returns>
        bool Rollback(Leaf command, int times = 1)
        {
            int startIndex = command.HexaCode.Length - times * 2;

            if (startIndex >= 0)
            {
                command.HexaCode = command.HexaCode.Remove(startIndex);
                return true;
            }

            return false; // couldn't rollback the number of times specified
        }

        // max int32 value in hexadecimal is 0x7FFFFFFF


        private static readonly byte[] Empty = new byte[0];

        // In a hexadecimal string, one byte is represented two hexadecimal characters. A hexadecimal character has a value of (A-F, 0-9).
        // e.g. string “01FFA0” is equivalent to byte[] { 1, 255, 160 }
        public static byte[] ConvertToByteArray(string value)
        {
            byte[] bytes = null;
            if (String.IsNullOrEmpty(value))
            {
                bytes = Empty;
            }
            else
            {
                int string_length = value.Length;
                int character_index = (value.StartsWith("0x", StringComparison.Ordinal)) ? 2 : 0; // Does the string define leading HEX indicator '0x'. Adjust starting index accordingly.               
                int number_of_characters = string_length - character_index;

                bool add_leading_zero = false;
                if (0 != (number_of_characters % 2))
                {
                    add_leading_zero = true;

                    number_of_characters += 1;  // Leading '0' has been striped from the string presentation.
                }

                bytes = new byte[number_of_characters / 2]; // Initialize our byte array to hold the converted string.

                int write_index = 0;
                if (add_leading_zero)
                {
                    bytes[write_index++] = FromCharacterToByte(value[character_index], character_index);
                    character_index += 1;
                }

                for (int read_index = character_index; read_index < value.Length; read_index += 2)
                {
                    byte upper = FromCharacterToByte(value[read_index], read_index, 4);
                    byte lower = FromCharacterToByte(value[read_index + 1], read_index + 1);

                    bytes[write_index++] = (byte)(upper | lower);
                }
            }

            return bytes;
        }

        private static byte FromCharacterToByte(char character, int index, int shift = 0)
        {
            byte value = (byte)character;
            if (((0x40 < value) && (0x47 > value)) || ((0x60 < value) && (0x67 > value)))
            {
                if (0x40 == (0x40 & value))
                {
                    if (0x20 == (0x20 & value))
                        value = (byte)(((value + 0xA) - 0x61) << shift);
                    else
                        value = (byte)(((value + 0xA) - 0x41) << shift);
                }
            }
            else if ((0x29 < value) && (0x40 > value))
                value = (byte)((value - 0x30) << shift);
            else
                throw new InvalidOperationException(String.Format("Character '{0}' at index '{1}' is not valid alphanumeric character.", character, index));

            return value;
        }

        #endregion

    }
}
