﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultimodalFramework
{
    /// <summary>
    /// The 'Composite' class which in this case will be a 'Gesture'
    /// </summary>
    public class Composite : Component, IEnumerable
    {
        public readonly List<Component> _children = new List<Component>();

        #region Constructor

        public Composite(string name) : base (name)
        {
        }

        #endregion

        #region Abstract Methods Implementation

        /// <summary>
        /// Attemps to Add a new Component
        /// </summary>
        /// <param name="c">Component to Add</param>
        /// <returns>Added or Found node</returns>
        public override Component Add(Component c)
        {
            Component search = _children.Find(x => x.Name.Equals(c.Name));
            if (search == null)
            {
                _children.Add(c);
                return c;
            }
            return search;
        }

        public override void Remove(Component c)
        {
            _children.Remove(c);
        }

        public override void Display(int depth)
        {
            Console.WriteLine(new String('-', depth) + Name);

            // Recursively display child nodes
            foreach (Component component in _children)
            {
                component.Display(depth + 2);
            }
        }

        #endregion

        #region IEnumerable Implementation

        public IEnumerator GetEnumerator()
        {
            foreach (Component c in _children)
            {
                // Return the current element and then on next function call 
                // resume from next element rather than starting all over again
                yield return c;
            }
        }

        #endregion
    }
}
