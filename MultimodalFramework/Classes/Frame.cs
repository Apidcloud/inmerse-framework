﻿using System;

namespace MultimodalFramework
{
    /// <summary>
    /// Frame also works as a workaround to accept every data structure in the Framework independently of the device SDK
    /// </summary>
    [Serializable]
    public class Frame
    {
        #region Properties

        public long Id { get; protected set; }

        #endregion

        #region Constructor

        public Frame(long id)
        {
            this.Id = id;
        }

        #endregion
    }
}
