﻿using System;

namespace MultimodalFramework.Ontologies
{
    [Serializable]
    public class OntologicalPalm
    {
        #region Properties

        public float[] Velocity { get; private set; }

        public float[] Normal { get; private set; }

        public float[] Position { get; private set; }

        public float[] Direction { get; private set; }

        #endregion

        #region Constructors

        // Position is the only needed for now, I think? Although that is expected to work on Unity only. Not sure outside.
        public OntologicalPalm(float[] position, float[] direction = null, float[] normal = null, float[] velocity = null)
        {
            if (position.Length != 3)
                throw new ArgumentOutOfRangeException("position", "This argument must have 3 bone positions (xyz).");

            this.Position = position;
            this.Direction = direction;
            this.Normal = normal;
            this.Velocity = velocity;
        }

        #endregion
    }
}
