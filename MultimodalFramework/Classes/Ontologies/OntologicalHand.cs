﻿using System;

namespace MultimodalFramework.Ontologies
{
    [Serializable]
    public class OntologicalHand
    {
        #region Properties

        public bool IsLeft { get; private set; }

        public bool IsRight { get { return !IsLeft; } }

        public OntologicalBasicFinger[] Fingers { get; private set; }
        public OntologicalPalm Palm { get; private set; }

        #endregion

        #region Constructors

        public OntologicalHand(OntologicalBasicFinger[] fingers, OntologicalPalm palm, bool isLeft)
        {
            if (fingers.Length != 5)
                throw new ArgumentOutOfRangeException("fingers", "Exactly 5 fingers are needed (information wise, even if invalid--for handicaped cases).");

            this.Fingers = fingers;
            this.Palm = palm;
            this.IsLeft = IsLeft;
        }

        #endregion
    }
}
