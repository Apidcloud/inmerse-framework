﻿using System;

namespace MultimodalFramework.Ontologies
{
    [Serializable]
    public class OntologicalBasicFinger
    {
        #region Properties

        public float[][] BonesPositions { get; private set; }
        public float[][] BonesDirections { get; private set; }
        public long Id { get; private set; }

        #endregion

        /// All fingers contain 4 bones that make up the anatomy of the finger.
        /// But metacarpal is not really needed for its representation, thus we only consider 3 here.
        /// Bones are then ordered from base to tip, indexed from 0 to 2 (proximal, intermediate, and distal, respectively).
        public OntologicalBasicFinger(float[][] bonesPositions, float[][] bonesDirections, long fingerId)
        {
            if (bonesPositions.Length != 3)
                throw new ArgumentOutOfRangeException("bonesPositions", "This argument must have 3 bone positions (xyz).");
            if (bonesDirections.Length != 3)
                throw new ArgumentOutOfRangeException("bonesDirections", "This argument must have 3 bone directions (xyz).");
            if (bonesPositions[0].Length != 3 || bonesPositions[1].Length != 3 || bonesPositions[2].Length != 3)
                throw new ArgumentOutOfRangeException("bonesPositions", "Each bone must have 3 float numbers representing xyz positions.");
            if (bonesDirections[0].Length != 3 || bonesDirections[1].Length != 3 || bonesDirections[2].Length != 3)
                throw new ArgumentOutOfRangeException("bonesDirections", "Each bone must have 3 float numbers representing xyz directions.");

            this.BonesPositions = bonesPositions;
            this.BonesDirections = bonesDirections;
            this.Id = fingerId;
        }
    }
}
