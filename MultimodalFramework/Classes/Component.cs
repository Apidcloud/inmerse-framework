﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultimodalFramework
{
    /// <summary>
    /// The 'Component' abstract class
    /// </summary>
    public abstract class Component
    {
        public string Name { get; private set; }

        #region Constructor

        public Component(string name)
        {
            Name = name;
        }

        #endregion

        #region Abstract Methods

        public abstract Component Add(Component c);
        public abstract void Remove(Component c);
        public abstract void Display(int depth);

        #endregion
    }
}
