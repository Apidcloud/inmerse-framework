﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultimodalFramework
{
    public class GestureDetection // UIGestureRecognizer; UISwipeGestureRecognizer
    {
        public delegate void EmptyEventHandler(); // change to IDEventHandler, sending the device id
        public delegate void StringEventHandler(string str);
        //public class Android
        //{
            public interface IGestureListener
            {
                event StringEventHandler OnGestureDetected;

                FrameBuffer FrameBuffer {get;}
            }

            #region Event Interfaces

            public interface ISwipeEvents
            {
                event EmptyEventHandler OnSwipeRight;
                event EmptyEventHandler OnSwipeLeft;
                event EmptyEventHandler OnSwipeUp;
                event EmptyEventHandler OnSwipeDown;
            }

            public interface IHandEvents
            {
                event EmptyEventHandler OnGrab;
                event EmptyEventHandler OnOpen;
                event EmptyEventHandler OnPinch;
            }

            public interface ITapEvents
            {
                event EmptyEventHandler OnSingleTap;
                event EmptyEventHandler OnDoubleTap;
            }

            #endregion

            #region Explicit Gesture Interfaces

            public interface IGrabListener : IGestureListener, IHandEvents
            {
                void Grab();
                void Open();
                void Pinch();
            }

            public interface ISwipeListener : IGestureListener, ISwipeEvents // No need for events here?
            {
                void SwipeRight();
                void SwipeLeft();
                void SwipeUp();
                void SwipeDown();
            }

            public interface ITapListener : IGestureListener, ITapEvents
            {
                void SingleTap();
                void DoubleTap();
            }

            #endregion

        //}

        //public class iOS
        //{
        //    public interface OnSwipeListener
        //    {
        //        void OnSwipeRight();
        //        void OnSwipeLeft();
        //        void OnSwipeUp();
        //        void OnSwipeDown();
        //    }

        //    public interface OnTapListener
        //    {
        //        void OnSingleTap();
        //        void OnDoubleTap();
        //    }

        //    public interface OnGestureListener : OnSwipeListener, OnTapListener
        //    {

        //    }
        //}
    }
}
