﻿using Microsoft.ML.Data;

namespace HandGesturesDecoder
{
    internal class GestureData
    {
        [VectorType(30)]
        [ColumnName("mulDense_input")]
        public float[] Values { get; set; }
    }
}
