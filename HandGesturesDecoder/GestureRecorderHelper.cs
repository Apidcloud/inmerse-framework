﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;

namespace HandGesturesDecoder
{
    public static class GestureRecorderHelper
    {
        #region Public Static Methods

        public static int GetAttemptNumber(string fullFilePath, int attemptColumnId, string separatorChar = ",")
        {
            // if file does not exist, then it is attempt number 1
            if (!File.Exists(fullFilePath))
                return 1;

            // need valid arguments
            if (attemptColumnId < 0 || separatorChar.Length < 0)
                throw new ArgumentException("The argument attemptColumnId cannot be negative, and separatorChar must have at least 1 character.");

            string[] columns = File.ReadAllLines(fullFilePath).Last().Split(separatorChar[0]);

            // check for Id validity
            if (attemptColumnId > columns.Length - 1 || columns.Length - 1 < attemptColumnId)
                throw new ArgumentOutOfRangeException("attemptColumnId", "This argument must be within the range of the number of columns split by a '" + separatorChar + "' character.");

            int foundAttempt;
            string matchResult = columns[attemptColumnId];

            // check for match validity
            if (!Int32.TryParse(matchResult, out foundAttempt))
                throw new InvalidCastException("The column with the specified Id is not an integer. It returns " + matchResult + " instead.");

            // return next attempt number
            return ++foundAttempt;
        }

        public static string[] GetMergedFeaturesLabels(float[] features, string[] featuresLabels)
        {
            if (features == null || featuresLabels == null || features.Length == 0 || featuresLabels.Length == 0)
                throw new ArgumentException("features", "Features and its labels cannot be null or empty.");

            string[] allFeaturesLabels = new string[] { };

            // guarantee it is a multiple
            if (features.Length % featuresLabels.Length != 0)
                throw new ArgumentOutOfRangeException("features", "Make sure features array size is a multiple of the features labels size.");

            while (allFeaturesLabels.Length < features.Length)
            {
                allFeaturesLabels = allFeaturesLabels.Concat(featuresLabels).ToArray();
            }

            return allFeaturesLabels;
        }

        public static string[] GetMergedMetadataLabels(object[] metadata, string[] metadataLabels)
        {
            // no metadata labels needed
            if (metadata == null || metadata.Length == 0)
                return null;

            // if there is metadata, however, these sizes must match
            if (metadataLabels != null && metadataLabels.Length != metadata.Length)
                throw new ArgumentOutOfRangeException("metadata", "Make sure that both metadata size and metadata labels size match.");

            return metadataLabels;
        }

        public static string GetMergedFileColumnsLabels(string[] featuresLabels, string[] metadataLabels = null, string separatorChar = ",")
        {
            string allLabels = string.Empty;

            if (metadataLabels != null)
            {
                allLabels += string.Join(separatorChar, metadataLabels);
                allLabels += separatorChar;
            }

            allLabels += string.Join(separatorChar, featuresLabels);

            return allLabels;
        }

        public static void SaveColumnsLabels(string columnsLabels, string fullFilePath)
        {
            string lineToSave = columnsLabels + Environment.NewLine;
            File.AppendAllText(fullFilePath, lineToSave);
        }

        public static void SaveHandData(float[] features, string fullFilePath, string separatorChar = ",", params object[] metadata)
        {
            if (features == null || features.Length == 0)
                throw new ArgumentNullException("features", "There must be at least one feature.");

            string data = string.Empty;

            // create metadata columns if applicable
            if (metadata != null)
                data += string.Join(separatorChar, metadata);

            // "G9" format specifier ensures that the original Single value successfully round-trips
            string handData = string.Join(separatorChar, features.Select(f => f.ToString("G9", CultureInfo.InvariantCulture)).ToArray());

            string lineOfData = data + separatorChar + handData + Environment.NewLine;

            File.AppendAllText(fullFilePath, lineOfData);
        }

        #endregion
    }
}
