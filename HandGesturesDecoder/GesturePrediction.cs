﻿using Microsoft.ML.Data;

namespace HandGesturesDecoder
{
    internal class GesturePrediction
    {
        [ColumnName("out")]
        public float[] PredictedGesture { get; set; }
    }
}
