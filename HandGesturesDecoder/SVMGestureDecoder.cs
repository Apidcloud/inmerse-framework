﻿using MultimodalFramework;
using MultimodalFramework.Ontologies;
using MultimodalFramework.Utils;
using System;
using System.IO;
using System.Linq;
using System.Diagnostics;
using Microsoft.ML;
using Microsoft.ML.Transforms.Onnx;

namespace HandGesturesDecoder
{
    public class SVMGestureDecoder: Decoder
    {
        #region Consts

        // probability (confidence) needed to consider a gesture valid
        private const float PROBABILITY_THRESHOLD = 0.7f;

        // dont forget to rebuild this project and its consumer when changing these!
        private const int RECORDING_SECONDS = 20;
        private const int SUBJECT_ID = 0;
        private const int GESTURE_ID = 4;

        private const bool SHOULD_RECORD = false;
        #endregion

        #region Fields

        #region Prediction Model related

        private readonly MLContext _modelContext = null;
        private readonly PredictionEngine<GestureData, GesturePrediction> _predictionEngine = null;

        //Gesture dictionary
        private readonly string[] _gestureLabels;

        #endregion

        private int _tempAttemptNumber = -1;
        private bool _columnsNamesAdded = false;
        private Stopwatch _recordingStopWatch = null;

        #endregion

        #region Constructors

        public SVMGestureDecoder(string decoderSupportedGesturesFilepath): base(decoderSupportedGesturesFilepath)
        {
            this._gestureLabels = CodenameHelper.GetGesturesCodenames(decoderSupportedGesturesFilepath).ToArray();

            this._modelContext = new MLContext();
            this._predictionEngine = this.LoadModel(this._modelContext, "./", "inmerseNNGestureNormalised_5gestures.onnx");
        }

        #endregion

        private PredictionEngine<GestureData, GesturePrediction> LoadModel(MLContext context, string path, string modelFilename)
        {
            // Load Trained Model
            // be sure to change the output and input columns names accordingly in GestureData.cs and GesturePrediction.cs
            // you can inspect the layer names, and the input and output through Netron, or model.summary in the python script
            OnnxScoringEstimator onnxPredictionPipeline = context.Transforms.ApplyOnnxModel(
                                                                            outputColumnNames: new[] { "out" }, 
                                                                            inputColumnNames: new[] { "mulDense_input" }, 
                                                                            System.IO.Path.Combine(path, modelFilename));

            var emptyDv = context.Data.LoadFromEnumerable(new GestureData[] { });
            OnnxTransformer transformer =  onnxPredictionPipeline.Fit(emptyDv);

            var onnxPredictionEngine = context.Model.CreatePredictionEngine<GestureData, GesturePrediction>(transformer);

            return onnxPredictionEngine;
        }

        public override void OnFrameReady(Frame frame)
        {
            // Decoders are data specific, so they always need to know what type of data is arriving to perform predictions
            // check what time of frame
            // ontological hands
            // ontological bodies
            // etc

            // flag to prevent predictions when only recording
            bool shouldRecord = SHOULD_RECORD;

            if (shouldRecord && _recordingStopWatch == null)
            {
                Console.WriteLine("Starting Stopwatch to record for " + RECORDING_SECONDS.ToString() + " seconds.");
                _recordingStopWatch = new Stopwatch();
                _recordingStopWatch.Start();
                shouldRecord = false;
            }

            if (_recordingStopWatch != null && _recordingStopWatch.Elapsed >= TimeSpan.FromSeconds(RECORDING_SECONDS))
            {
                _recordingStopWatch.Stop();
                _recordingStopWatch = null;
                Console.WriteLine("Stopping Stopwatch.");
                // just a way of telling the main consumer that the recording has ended
                this.InvokeDecoderGestureDetectedEvent("RecordingEnded");
            }
                
            switch (frame)
            {
                case OntologicalHandsFrame handsFrame:
                    if (_recordingStopWatch != null)
                    {
                        // maybe add some logic or UI to change the subject and gesture Ids
                        this.RecordSubjectGesture("./", SUBJECT_ID, GESTURE_ID, handsFrame, "handGestureDataNormalised");
                    } 

                    if (!SHOULD_RECORD)
                    {
                        string predictedGesture = this.PredictGesture(handsFrame);
                        if (predictedGesture != null)
                            this.InvokeDecoderGestureDetectedEvent(predictedGesture.ToLower());
                    }

                    break;
                case OntologicalBodiesFrame bodiesFrame:
                    break;
                default:
                    break;
            }

            //Console.WriteLine("Received frame!");
            //this.InvokeDecoderGestureDetectedEvent("test gesture from decoder");
        }

        private string PredictGesture(OntologicalHandsFrame handsFrame)
        {
            if (_modelContext == null || _predictionEngine == null)
                return null;

            float[] inputData = this.GenerateHandData(handsFrame);

            if (inputData == null)
                return null;

            // Get Prediction
            GesturePrediction prediction = this._predictionEngine.Predict(new GestureData() { Values = inputData });

            //Console.WriteLine(prediction.PredictedGesture[0] + "::" + prediction.PredictedGesture[1] + "::" + prediction.PredictedGesture[2]);

            float maxPrediction = prediction.PredictedGesture.Max();

            if (maxPrediction < PROBABILITY_THRESHOLD)
                return null;

            int indexOf = Array.IndexOf(prediction.PredictedGesture, maxPrediction);

            if (indexOf < 0 || indexOf > this._gestureLabels.Length - 1)
                throw new IndexOutOfRangeException("The index of the predicted gesture is out of range.");

            return this._gestureLabels[indexOf];
        }

        public void RecordSubjectGesture(string path, int subjectId, int gestureId, OntologicalHandsFrame handsFrame, string filenamePrefix = "handData")
        {
            if (subjectId < 0 || gestureId < 0)
                throw new ArgumentException("SubjectId and GestureId cannot be negative.");

            float[] features = this.GenerateHandData(handsFrame);

            if (features == null)
                return;

            string separatorChar = ",";
            string fullFilePath = this.GetSubjectGestureFullFilePath(path, subjectId, gestureId, filenamePrefix);

            if (_tempAttemptNumber == -1)
                _tempAttemptNumber = GestureRecorderHelper.GetAttemptNumber(fullFilePath, 2, separatorChar);

            object[] metadata = new object[] { subjectId, gestureId, _tempAttemptNumber };

            // add columns names first if it is the first attempt
            if (_tempAttemptNumber == 1 && !_columnsNamesAdded)
            {
                string[] featuresLabels = GestureRecorderHelper.GetMergedFeaturesLabels(features, this.GetFeaturesLabels());
                string[] metadataLabels = GestureRecorderHelper.GetMergedMetadataLabels(metadata, this.GetMetadataLabels());

                string allLabels = GestureRecorderHelper.GetMergedFileColumnsLabels(featuresLabels, metadataLabels, separatorChar);
                GestureRecorderHelper.SaveColumnsLabels(allLabels, fullFilePath);

                this._columnsNamesAdded = true;
            }

            // add data
            GestureRecorderHelper.SaveHandData(features, fullFilePath, separatorChar, metadata);
        }

        /// <summary>
        /// Generate first hand data if available
        /// </summary>
        /// <param name="handsFrame"></param>
        /// <returns></returns>
        public float[] GenerateHandData(OntologicalHandsFrame handsFrame)
        {
            if (handsFrame.Hands.Length == 0)
                return null;

            return this.GenerateHandData(handsFrame.Hands[0]);
        }

        /// <summary>
        /// Generate specific hand data if available
        /// </summary>
        /// <param name="hand"></param>
        /// <returns></returns>
        public float[] GenerateHandData(OntologicalHand hand)
        {
            if (hand == null)
                return null;

            // each finger bone xyz
            float[][] bone2 = new float[5][];
            float[][] bone1 = new float[5][];

            // 30 features
            float[] currentData = new float[30];
            
            float[] palmPlaneNormal = hand.Palm.Normal;
            float[] palmPlaneUp = hand.Palm.Direction;
            float[] palmPlaneRight = Vector3Helper.Vector3CrossProduct(palmPlaneNormal, palmPlaneUp);

            for (int i = 0; i < 5; i++)
            {
                // xyz
                bone2[i] = new float[3];
                bone1[i] = new float[3];

                // distal bone
                float[] palmBone2 = Vector3Helper.Vector3Subtraction(hand.Fingers[i].BonesPositions[2], hand.Palm.Position);
                // intermediate bone
                float[] palmBone1 = Vector3Helper.Vector3Subtraction(hand.Fingers[i].BonesPositions[1], hand.Palm.Position);

                // xyz
                bone2[i][0] = Vector3Helper.Vector3MagnitudeSqr(Vector3Helper.Vector3ProjectOnPlane(palmBone2, palmPlaneRight));
                bone2[i][1] = Vector3Helper.Vector3MagnitudeSqr(Vector3Helper.Vector3ProjectOnPlane(palmBone2, palmPlaneNormal));
                bone2[i][2] = Vector3Helper.Vector3MagnitudeSqr(Vector3Helper.Vector3ProjectOnPlane(palmBone2, palmPlaneUp));
                // xyz
                bone1[i][0] = Vector3Helper.Vector3MagnitudeSqr(Vector3Helper.Vector3ProjectOnPlane(palmBone1, palmPlaneRight));
                bone1[i][1] = Vector3Helper.Vector3MagnitudeSqr(Vector3Helper.Vector3ProjectOnPlane(palmBone1, palmPlaneNormal));
                bone1[i][2] = Vector3Helper.Vector3MagnitudeSqr(Vector3Helper.Vector3ProjectOnPlane(palmBone1, palmPlaneUp));

                // xyz
                currentData[i * 6] = bone2[i][0];
                currentData[i * 6 + 1] = bone2[i][1];
                currentData[i * 6 + 2] = bone2[i][2];
                // xyz
                currentData[i * 6 + 3] = bone1[i][0];
                currentData[i * 6 + 4] = bone1[i][1];
                currentData[i * 6 + 5] = bone1[i][2];
            }

            return currentData;
        }

        private string GetSubjectGestureFullFilePath(string basePath, int subjectId, int gestureId, string filenamePrefix)
        {
            string filename = string.Join("_", filenamePrefix, "S" + subjectId, "G" + gestureId);
            filename += ".txt";

            return Path.Combine(basePath, filename);
        }

        private string[] GetFeaturesLabels()
        {
            string[] featuresLabels = new string[6];

            featuresLabels[0] = "DistalBonePalmProjectionX";
            featuresLabels[1] = "DistalBonePalmProjectionY";
            featuresLabels[2] = "DistalBonePalmProjectionZ";
            featuresLabels[3] = "IntermediateBonePalmProjectionX";
            featuresLabels[4] = "IntermediateBonePalmProjectionY";
            featuresLabels[5] = "IntermediateBonePalmProjectionZ";

            return featuresLabels;
        }

        private string[] GetMetadataLabels()
        {
            string[] metadataLabels = new string[3];

            metadataLabels[0] = "SubjectId";
            metadataLabels[1] = "GestureId";
            metadataLabels[2] = "AttemptNumber";

            return metadataLabels;
        }
    }
}
